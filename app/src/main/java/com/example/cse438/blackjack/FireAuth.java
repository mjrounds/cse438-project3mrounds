package com.example.cse438.blackjack;
import android.app.ActionBar;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import java.util.*;
import com.firebase.client.Firebase;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class FireAuth extends AppCompatActivity {

    private EditText email;
    private EditText password;
    private Button signIn;
    private Button signUp;
    private FirebaseAuth fAuth;
    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseFirestore fStore;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        signIn = findViewById(R.id.signIn);
        signUp = findViewById(R.id.signUp);
        fAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();
        //fAuth.signOut();
        authListener = new FirebaseAuth.AuthStateListener()
        {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth)
            {
                if(fAuth.getCurrentUser()!=null)
                {
                    startActivity(new Intent(FireAuth.this, AccountActivity.class));

                }
            }
        };
        signIn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {

                signIn();
            }
        });
        signUp.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                register();
            }
        });
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        fAuth.addAuthStateListener(authListener);
    }
    private void signIn()
    {
        final String e = email.getText().toString().trim();
        final String pass = password.getText().toString();
        if((TextUtils.isEmpty(e))||(TextUtils.isEmpty(pass)))
        {
            TextView err = findViewById(R.id.error);
            err.setText("One or more fields is empty");
        }
        else
        {
            fAuth.signInWithEmailAndPassword(e,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {

                    if(task.isSuccessful()==false)
                    {
                        TextView err = findViewById(R.id.error);
                        err.setText("Sign In problem");
                    }
                }
            });
        }
    }
    private void register()
    {
        final String e = email.getText().toString().trim();
        final String pass = password.getText().toString();
        if((TextUtils.isEmpty(e))||(TextUtils.isEmpty(pass)))
        {
            TextView err = findViewById(R.id.error);
            err.setText("One or more fields is empty");
            return;
        }
        else if(Patterns.EMAIL_ADDRESS.matcher(e).matches()==false)
        {
            TextView err = findViewById(R.id.error);
            err.setText("Please enter valid email");
            return;
        }
        else if(pass.length() < 8)
        {
            TextView err = findViewById(R.id.error);
            err.setText("password should be at least 8 characters");
            return;
        }
        else
        {
            fAuth.createUserWithEmailAndPassword(e,pass).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                @Override
                public void onSuccess(AuthResult authResult) {
                    TextView err = findViewById(R.id.error);
                    err.setText("Succsfully created account");
                    Map<String,String> users =  new HashMap<>();
                    users.put("name", e);
                    users.put("numLosses", "0");
                    users.put("numWins", "0");
                    fStore.collection("users").add(users);
                }
            });
        }

        return;


    }


}
