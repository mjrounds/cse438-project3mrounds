package com.example.cse438.blackjack

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.google.firebase.firestore.FirebaseFirestore
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import android.widget.Button
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import android.view.View
import android.support.v7.widget.RecyclerView
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import android.support.v7.widget.RecyclerView.LayoutManager
import com.google.firebase.database.FirebaseDatabase

import com.google.firebase.database.Query
import com.google.firebase.firestore.QuerySnapshot

class LeaderBoard : AppCompatActivity(){

    private var adapter: RecyclerViewAdapter? = null
    private var fStore: FirebaseFirestore = FirebaseFirestore.getInstance()
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private  var users:  Task<QuerySnapshot> = fStore.collection("users").get()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leader_board)
        val listNames:ArrayList<String> = ArrayList()

        fStore.collection("users")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    var s:String = document.get("name").toString()
                    Log.d("name: ",s)
                    listNames.add(s)
                    val recyclerView:RecyclerView = findViewById(R.id.my_recycler_view)
                    recyclerView.layoutManager = LinearLayoutManager(this)
                    adapter = RecyclerViewAdapter(this, listNames)
                    recyclerView.setAdapter(adapter)
                }
            }
            .addOnFailureListener { exception ->
                listNames.add("cannot load leaderboard")
            }

        val recyclerView:RecyclerView = findViewById(R.id.my_recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(this, listNames)
        recyclerView.setAdapter(adapter)

    }
}
